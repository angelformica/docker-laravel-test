# How to Install

## First
* clone the repo
## Then, run the following commands
* docker-compose build && docker-compose up -d
* docker-compose run --rm composer install
* cp ./src/.env.example ./src/.env
* docker-compose run --rm artisan key:generate
* docker-compose run --rm artisan config:cache

## Compiling assets
### inside the docker instance
* docker-compose run --rm npm install
* docker-compose run --rm npm run dev
### or outside the docker instance (faster)
* npm install
* npm run dev

## Composer
Needs to be executed inside the docker instance, like this:
* docker-compose run --rm composer require package-user-name/package-name

or:
* docker-compose run --rm composer update

## Artisan
Also needs to be executed inside the docker instance, like this:
* docker-compose run --rm artisan config:cache

or:
* docker-compose run --rm artisan key:generate

(You get the idear)



